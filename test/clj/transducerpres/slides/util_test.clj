(ns transducerpres.slides.util-test
    (:require [clojure.test :refer :all]
              [transducerpres.slides.util :refer [slide defslide markdown
                                                  defmarkdownslide clj-code
                                                  markdown-slide
                                                  fa]]))

(deftest test-slide
    (testing "simple slide"
        (let [expt [:section [:h1 "title"] ["body"]]
              res (slide [:h1 "title"] ["body"])]
            (is (= expt res))
            ))
    (testing "slide with attibute"
        (let [expt [:section {:class "boo"} [:h1 "title"] ["body"]]
              res (slide {:class "boo"} [:h1 "title"] ["body"])]
            (is (= expt res)))))

(deftest test-markdown
    (testing "simple slide"
        (let [expt [:script {:type "text/template"} "# title\nbody"]
              res (markdown "# title" "body")]
            (is (= expt res))
            )))

(deftest test-defslide
    (testing "simple case"
        (let [expt [:section [:h1 "title"] ["body"]]]
            (defslide test-slide
                [:h1 "title"]
                ["body"])
            (is (= expt test-slide))
            )))

(deftest test-defmarkdownslide
    (testing "markdown-slide"
        (let [expt [:section {:data-markdown true} [:script {:type "text/template"}  "# title\nbody"]]]
            (is (= expt (markdown-slide "# title" "body") ))))

    (testing "simple case"
        (let [expt [:section {:data-markdown true} [:script {:type "text/template"}  "#title\nbody"]]]
            (defmarkdownslide test-slide
                "#title"
                "body")
            (is (= expt test-slide))
            )))

(deftest test-clj-code
    (testing "simple case"
        (let [expt [:pre [:code {:class "clojure"} "(+ 1 1)\n(+ 2 2)"]]]
            (is (= (clj-code "(+ 1 1)" "(+ 2 2)") expt)))))

(deftest test-font-awsome
    (testing "simple case"
        (let [expt [:i {:class "fa fa-map" :aria-hidden "true"}]]
            (is (= (fa "map") expt)))))
