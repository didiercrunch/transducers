(ns transducerpres.doo-runner
  (:require [doo.runner :refer-macros [doo-tests]]
            [transducerpres.core-test]))

(doo-tests 'transducerpres.core-test)
