(ns transducerpres.handler
  (:require [compojure.core :refer [GET defroutes]]
            [compojure.route :refer [not-found resources]]
            [hiccup.page :refer [include-js include-css html5]]
            [transducerpres.middleware :refer [wrap-middleware]]
            [transducerpres.presentation :refer [presentation]]
            [config.core :refer [env]]))

(def slides
    [:div.reveal
        (vec (cons :div.slides presentation))])

(defn head []
    [:head
        [:meta {:charset "utf-8"}]
        [:meta {:name "viewport"
                :content "width=device-width, initial-scale=1"}]
        (include-css "/reveal/css/reveal.css")
        (include-css "/font-awesome-4.7.0/css/font-awesome.css")
        (include-css "/highlight/styles/darcula.css")
        (include-css "/reveal/css/theme/night.css") ])

(defn loading-page []
    (html5
        (head)
        [:body
            slides
            (include-js "/reveal/js/reveal.js")
            (include-js "/reveal/plugin/markdown/marked.js")
            (include-js "/reveal/plugin/markdown/markdown.js")
            (include-js "/reveal/plugin/math/math.js")
            (include-js "/reveal/plugin/highlight/highlight.js")
            [:script "Reveal.initialize({ slideNumber: true, history: false, rollingLinks: true });"]
            [:script "hljs.initHighlightingOnLoad();"]
        ]))


(defroutes routes
  (GET "/" [] (loading-page))
  (GET "/about" [] (loading-page))

  (resources "/")
  (not-found "Not Found"))

(def app (wrap-middleware #'routes))
