(ns transducerpres.slides.transducer-function
    (:require [transducerpres.slides.util :refer [defslide defmarkdownslide markdown-part clj-code img]]))


(defslide intro-slide
    (markdown-part
        "#### transducer functions!!")
    (img "devil.png"))

(defslide we-start-from-where-we-were
    (markdown-part "#### We start from where we were...")
    (clj-code
        "(defn my-map [f coll]"
        "    (reduce #(conj %1 (f %2)) [] coll))")
    (clj-code
        "(defn my-filter [pred coll]"
        "    (reduce #(if (pred %2) (conj %1 %2) %1) [] coll))")
    (clj-code
        "(defn my-mapcat [pred coll]"
        "    (reduce (fn [r x](reduce conj r (f x))) [] coll))")
        )

(defslide we-parameterize-conj
    (markdown-part "#### And we extract the essence of our functions")
    (clj-code
        "(defn mapping [f]"
        "    (fn [step]"
        "        (fn [res x] (step res (f x)))))")
    (clj-code
        "(defn filtering [pred]"
        "    (fn [step]"
        "        (fn [res x] (if (pred x) (step res x) res))))")
    (clj-code
        "(def cat"
        "    (fn [step]"
        "        (fn [res x] (reduce step res x))))")
    (clj-code
        "(defn mapcatting [f]"
        "    (comp cat (mapping f)))"))

(defslide enjoy-writing-our-function-the-same-way
    (markdown-part "#### And we can now write our functions in the same way")
    (clj-code
        "(defn my-map [f coll]"
        "    (reduce ((mapping f) conj) [] coll))")
    (clj-code
        "(defn my-filter [pred coll]"
        "    (reduce ((filtering pred) conj) [] coll))")
    (clj-code
        "(defn my-mapcat [f coll]"
        "    (reduce ((mapcatting f) conj) [] coll))"))

(defslide note-on-mapcatting
    (markdown-part "#### Note on mapcat")
    (markdown-part "we have defined...")
    (clj-code
        "(defn mapping [f]"
        "    (fn [step]"
        "        (fn [res x] (step res (f x)))))")
    (clj-code
        "(def cat"
        "    (fn [step]"
        "        (fn [res x] (reduce step res x))))")
    (clj-code
        "(defn mapcatting [f]"
        "    (comp cat (mapping f)))")
    [:p "for محمد بن موسی خوارزمی sake why is cat applied before map"])

(defslide developping-mapcatting
    (markdown-part "#### develop mapcattig the reason is trivial")
    (clj-code
        "(fn [res x] (reduce mapping-step res x))")
    (clj-code
        "(fn [res x] (reduce (fn [res x] (step res (f x))) res x))"))

(defslide composition-for-human
    (markdown-part "#### but for humans...")
    (clj-code
        "(def trans-1"
        "    (fn [step](fn [res x] (step res (+ 2 x)))))")
    (clj-code
        "(def trans-2"
        "    (fn [step](fn [res x] (step res (* 3 x)))))")
    (clj-code
        "(def trans-3"
        "    (comp trans-1 trans-2))")

    (clj-code
        "(fn [res x] (fn [res x] (step res (* 3 x))) res (+ 2 x)))"))

(defmarkdownslide what-transducers-can-do
    "#### So transducers are..."
    "*  know nothing about the process they modify"
    "    * reducing functionnality fully encapsulates"
    "*  may call step 0, 1 or many times"
    "*  must pass previous result as next r otherwise must know nothing of r"
    "*  can transform input args")


(defslide transducer-function-slides
    intro-slide
    we-start-from-where-we-were
    we-parameterize-conj
    enjoy-writing-our-function-the-same-way
    note-on-mapcatting
    developping-mapcatting
    composition-for-human
    what-transducers-can-do)
