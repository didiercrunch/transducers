(ns transducerpres.slides.analogy
    (:require [transducerpres.slides.util :refer [defslide defmarkdownslide clj-code markdown-slide markdown-part img]]))

(defmarkdownslide title
    "## Rich Hickey's analogy"
    "> talk: https://www.youtube.com/watch?v=6mTbuzafcII")

(defmarkdownslide the-analogy
    "### To load an airplane you need to...")

(defslide getting-the-luggages
    [:h4 "1 : You start with some luggage crates"]
    (img "crates.jpg"))

(defslide unpack-the-crates
    [:h4 "2 : unpack the luggaes from the crates"]
    (img "unload.jpg"))

(defslide remove-bags
    [:h4 "3 : remove bags that contain laptop"]
    (img "laptop-bag.jpg"))

(defslide label-heavy-bags
    [:h4 "4 : label heavy bags"]
    (img "heavy-bag.jpeg"))

(defslide load-the-airplane
    [:h4 "5 : load the airplane"]
    (img "load-airplane.jpg"))

(defmarkdownslide recap-the-analogy
    "### Recap the analogy"
    "|  Physical action/thing          | Function/data structure |"
    "|:--------------------------------|:------------------------|"
    "|crates of bags                   | seq of seq of Map       |"
    "|un-pack the crates               | `mapcat`                |"
    "|remove bags that contains laptop | `filtering`             |"
    "|label heavy bags                 | `map`                   |"
    "|airplane                         | sequence                |")

(defslide lesson-from-the-analogy
    [:h3 "Lessons from the analogy"])

(defmarkdownslide lesson-from-the-analogy-1
    "#### Lesson number 1"
    "> The origin and destination of the bags do not matter"
    "*  receiving the crates in a truck or on a conveyor belt is the same"
    "*  Loading a ship or an airplane is the same thing")

(defmarkdownslide lesson-from-the-analogy-2
    "#### Lesson number 2"
    "> There is no need to place the bags on a conveyor belt between each step"
    )

; Rich Hickey
(defslide rich-hickey-analogy
    title
    the-analogy
    getting-the-luggages
    unpack-the-crates
    remove-bags
    label-heavy-bags
    load-the-airplane
    recap-the-analogy
    lesson-from-the-analogy
    lesson-from-the-analogy-1
    lesson-from-the-analogy-2
    )
