(ns transducerpres.slides.arities
    (:require [transducerpres.slides.util :refer [defslide defmarkdownslide markdown-part clj-code img]]))

(defmarkdownslide intro-slide
    "#### The transducer function **must** support two more arities")

(defmarkdownslide one-arity
    "#### completion of process"
    "When a process terminates, the arity one transducer function"
    "is called to allow transducer to do extra things")

(defmarkdownslide zero-arity
    "#### initial reducing value"
    "To allow initial values, trasducer **must** have an arity"
    "zero function that calls the step function without any argument")

(defslide real-world-example
    (markdown-part "real world example")
    (clj-code
        "(defn map [f]"
        "   (fn [rf]"
        "       (fn"
        "           ([] (rf))"
        "           ([result] (rf result))"
        "           ([result input]"
        "               (rf result (f input))))))"))

(defslide arities-slides
    intro-slide
    one-arity
    zero-arity
    real-world-example)
