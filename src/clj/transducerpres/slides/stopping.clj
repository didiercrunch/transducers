(ns transducerpres.slides.stopping
    (:require [transducerpres.slides.util :refer [defslide defmarkdownslide markdown-part clj-code img]]))

(defmarkdownslide intro-slide
    "#### Stopping transducing process")

(defmarkdownslide goal
    "#### goal"
    "*  transducers must be capable of calling 'completion'"
    "*  step functions must be capable of calling 'completion'")

(defmarkdownslide stopping-in-reduce
    "#### `reduce` already support stopping the process"
    "*  `(reduced 3)` wraps 3 in a Reduced wrapper"
    "*  `(reduced? x)` tells is a value is wrapped"
    "*  `(deref x)` or `@x` will get you the wrapped value")

(defmarkdownslide reduced-transducer
    "#### transducer **must** support the reduced \"interface\""
    "*   step functions can return reduced value"
    "*   if a transducer get a reduced value from a nested step,"
    "    it must never call that step function again with input")

(defmarkdownslide stopping-process
    "#### reducing process **must** the rules too"
    "*   if a step function returns a reduced value it"
    "    the processs must not supply anymore input"
    "    to the step function"
    "*   the dereference value is the final accumulated value")

(defslide take-while-example
    (markdown-part "#### take-while example")
    (clj-code
        "(defn take-while [pred]"
        "    (fn [step]"
        "         (fn [res x]"
        "             (if (pred x)"
        "                  (step res x)"
        "                  (reduced res)))))"))


(defslide stopping-slides
    intro-slide
    goal
    stopping-in-reduce
    stopping-process
    take-while-example
    )
