(ns transducerpres.slides.util)

(defn slide [& body]
    (vec (concat [:section] body)))

(defn div [& body]
    (vec (concat [:div] body)))

(defn markdown [& body]
    [:script {:type "text/template"} (clojure.string/join "\n" body)])

(defn markdown-slide [& body]
    (slide {:data-markdown true} (apply markdown body)))

(defn markdown-part [& body]
    (div {:data-markdown true} (apply markdown body)))

(defn clj-code [& body]
    [:pre [:code {:class "clojure"} (clojure.string/join "\n" body)]])

(defn python-code [& body]
    [:pre [:code {:class "python"} (clojure.string/join "\n" body)]])

(defmacro defslide [name & body]
    (list 'def name (cons `slide body)))

(defmacro defmarkdownslide [name & body]
    (list 'def name (cons `markdown-slide body)))

(defn fa [name]
    [:i {:class (str "fa fa-" name) :aria-hidden "true"}])

(defn img [name]
    [:img {:src (str "/images/" name)}])
