(ns transducerpres.slides.intro
    (:require [transducerpres.slides.util :refer [defslide]]))

(defslide intro-slide
    [:h1 "Transducers: a deep dive"])
