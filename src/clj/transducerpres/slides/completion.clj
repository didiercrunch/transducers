(ns transducerpres.slides.completion
    (:require [transducerpres.slides.util :refer [defslide defmarkdownslide markdown-part clj-code img]]))


(defmarkdownslide intro-slide
    "#### Completion can do thing at the very end")

(defmarkdownslide explanation
    "#### Arity one moment"
    "*  The completion of the process is done at arity one"
    "*  It **must** finish by calling the step function with the result (arity one)")

(defmarkdownslide going-to-sleep
    "#### Going to sleep..."
    "https://github.com/clojure/clojure/blob/clojure-1.9.0-alpha14/src/clj/clojure/core.clj#L7115")


(defslide completion-slides
    intro-slide
    explanation
    going-to-sleep)
