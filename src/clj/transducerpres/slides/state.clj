(ns transducerpres.slides.state
    (:require [transducerpres.slides.util :refer [defslide defmarkdownslide markdown-part clj-code img]]))

(defmarkdownslide intro-slide
    "#### Creating transducers that has state")

(defslide thousand-words-example
    (markdown-part "#### thousand words example")
    (clj-code
        "(defn take [n]"
        "     (fn [step]"
        "         (let [nv (volatile! n)]"
        "             (fn [result input]"
        "                 (let [n @nv"
        "                       nn (vswap! nv dec)"
        "                       result (if (pos? n) (step result input) result)]"
        "        (if (not (pos? nn))"
        "              (reduced result)"
        "              result))))))"
))


(defslide state-slides
    intro-slide
    thousand-words-example)
