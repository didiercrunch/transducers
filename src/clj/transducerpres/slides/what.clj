(ns transducerpres.slides.what
    (:require [transducerpres.slides.util :refer [defslide defmarkdownslide clj-code markdown-slide markdown-part fa]]))

(defslide title
    [:h3 "What is a Transducer?"])

(defmarkdownslide wikipedia-definition
    "#### According to Wikipedia"
    "> A **transducer** is a device that converts one form of energy to another"
    "> ..."
    ">Transducers are often employed at the boundaries...")

(defmarkdownslide clojuredoc-definition
    "#### According to clojuredoc"
    "A **transducer** (sometimes referred to as **xform** or **xf**) is a "
    "transformation from one reducing function to another")

(defmarkdownslide rich-hickey-definition
    "#### According to Rich Hickey "
    "*  extract the **essence** of map, filter et al"
    "*  away of the functions that transform sequences/collections"
    "*  so they can be use elsewhere"
    "*  recasting them as **process transformation**")

(defmarkdownslide does-not-help-much
    "#### That does not help much (the first time)"
    "Instead let take a look at what transducer try to solve")

(defslide example-selection
    (markdown-part
        "#### let's look at an example"
        "We have two options..."
        "1. look at code example"
        "2. look at a very nice analogy")
    (fa "question"))

(defslide classical-problem
    (markdown-part
        "#### Classical problem: $ \\sum_{n=0}^{N-1} 2 \\cdot n + 3 $")
    (clj-code
        "(defn classic-sum [N]"
        "    (let [all-numbers (range)"
        "          input (take N all-numbers)"
        "          times-two (map #(* 2 %) input)"
        "          times-two-plus-three (map #(+ 3 %) times-two)]"
        "        (reduce + 0 times-two-plus-three)))")
    (markdown-part
        "note: Here we produce many lists when we actually do not need any"))

(defslide classical-problem-with-channel
    (markdown-part
        "#### And the solution to a very similar problem requires a re-written")
    (clj-code
        "(require '[clojure.core.async :as async])")
    (clj-code
        "(defn classic-sum-channel [N]"
        "    (let [all-numbers (async/to-chan (range))"
        "          input (async/take N all-numbers)"
        "          times-two  (async/map< #(* 2 %) input)"
        "          times-two-plus-three  (async/map< #(+ 3 %) times-two)]"
        "    (async/reduce + 0 times-two-plus-three)))"
        ))

(defslide classical-problem-with-transducer
    (markdown-part
        "#### Transducers simplify all the above fiddling")
    (clj-code
        "(require '[clojure.core.async :as async])")
    (clj-code
        "(defn xform [N]         ;; transducers are usually named xform or xf"
        "   (comp (take N)"
        "         (map #(* 2 %))"
        "         (map #(+ 3 %))))"
        ""
        "(defn trans-classic-sum [N]"
        "    (transduce (xform N) + 0 (range)))"
        ""
        "(defn trans-classic-sum-channel [N]"
        "    (async/transduce (xform N) + 0 (async/to-chan (range))))"
        ))

(defmarkdownslide transducer-formal-definition
    "Hence, a **transducer** is a data pipeline \"functional interface\""
    "that is un aware of the underlying data structure.")

(defslide what-is-a-tranducer
        title
        wikipedia-definition
        clojuredoc-definition
        rich-hickey-definition
        does-not-help-much
        example-selection
        classical-problem
        classical-problem-with-channel
        classical-problem-with-transducer
        transducer-formal-definition)
