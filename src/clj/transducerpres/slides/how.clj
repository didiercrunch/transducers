(ns transducerpres.slides.how
    (:require [transducerpres.slides.util :refer [defslide defmarkdownslide
                                                  clj-code markdown-slide
                                                  markdown-part]]))

(defslide intro-slide
    [:h3 "How do we create transducers?"]
    [:h3 "How do we use transducers?"])

(defn- func-link-bullet [name]
    (str "* `" name "`"))

(def transducer-functions (clojure.string/split "map cat mapcat filter remove take take-while take-nth drop drop-while replace partition-by partition-all keep keep-indexed map-indexed distinct interpose dedupe random-sample" #" "))

(defn transducer-function-list []
    (let [n-cols 4
          rows             (partition n-cols transducer-functions)
          row-format       (str (reduce str "|" (repeat n-cols "%s|")) "\n")
          format-table-row (fn [columns] (apply format (cons row-format columns)))
          table-header     (str (format-table-row (repeat n-cols "  ")) (format-table-row (repeat n-cols "--")))
          table-rows-xform (map format-table-row)
          ret              (transduce table-rows-xform str  table-header rows)
          ]
        ret))

(defmarkdownslide how-to-create
    "#### How to create transducers?"
    ""
    "The majority of data processing method implement an arity that returns"
    "a transducer.  The following functions produce a transducer when the "
    "input collection is omitted:"
    ""
    (transducer-function-list))

(defmarkdownslide how-to-use
    "#### How to use a transducer?"
    ""
    "* **transduce** is can be use like reduce"
    "* **eduction** is used to create reducible/iterable"
    "* **into** is used to append data to an existing collection"
    "* **sequence** can create a sequence from transducer")

(defslide example-1
    (markdown-part
        "#### example 1")
    (clj-code
        "(def xform (map #(* 2 %)))"
        ""
        "(transduce xform + 0 (range 10))"
        ";; 90"
        ""
        "(eduction xform (range 10))"
        ";; (0 2 4 6 8 10 12 14 16 18) ; an eduction"
        ""))


(defslide example-1-part-2
    (markdown-part
        "#### example 1... part 2")
    (clj-code
        "(def xform (map #(* 2 %)))"
        ""
        "(into [1 2] xform (range 10))"
        ";; [1 2 0 2 4 6 8 10 12 14 16 18]"
        ""
        "(into (list 1 2) xform (range 10))"
        ";; (18 16 14 12 10 8 6 4 2 0 1 2)"
        ""
        "(sequence xform (range 10))"
        ";; (0 2 4 6 8 10 12 14 16 18) ; lazy sec"))

(defslide example-2
    (markdown-part
        "#### example 2")
    (clj-code
        "(def xform (comp"
        "                 (map #(* 2 %))"
        "                 (map #(+ 3 %))))"
        ""
        "(sequence xform (range 10))"
        ";;  (3 5 7 9 11 13 15 17 19 21)"))

(defslide example-2-weirdness
    (markdown-part
        "#### example 2... weirdness")
    (clj-code
        "(def xform (map (comp "
        "                     #(* 2 %)"
        "                     #(+ 3 %))))"
        ""
        "(sequence xform (range 10))"
        ";;  (6 8 10 12 14 16 18 20 22 24)"))


(defslide how-to-create-slides
    intro-slide
    how-to-create
    how-to-use
    example-1
    example-1-part-2
    example-2
    example-2-weirdness)
