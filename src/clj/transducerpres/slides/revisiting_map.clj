(ns transducerpres.slides.revisiting-map
    (:require [transducerpres.slides.util :refer [defslide markdown-part clj-code python-code]]))

(defslide intro-slide
    [:h2 "let's look at how we would implement the "
         [:em "map"] " function"])

(defslide naive-way
    (markdown-part "### Naive implementation of map")
    (python-code
        "def naive_map(fn, lst):"
        "    ret = [0] * len(lst)"
        "    for i in range(len(lst)):"
        "        ret[i] = fn(lst[i])"
        "    return ret")
    (markdown-part "> I have no idea how to convert that to clojure"))


(defslide clojuresque-way
    (markdown-part "### clojuresque way of implementing map")
        (clj-code
            "(defn my-map [f coll]"
            "    (loop [ret [] coll coll]"
            "        (if (empty? coll)"
            "            ret"
            "            (recur (conj ret (f (first coll))) (rest coll)))))"
            ))

(defslide conj-wirdness
    (markdown-part "### `conj` is a weird beast")
    (clj-code
        "(conj [1 2 3] 99)"
        ";; [1 2 3 99]")
    (clj-code
        "(conj '(1 2 3) 99)"
        ";; (99 1 2 3)")
    (clj-code
        "(conj #{1 2 3} 99)"
        ";; #{1 2 99 3}")
    (clj-code
        "(conj {:key1 1 :key2 2} [:key3 3])"
        ";; {:key1 1, :key2 2, :key3 3}"))

(defslide reduce-way
    (markdown-part "### A more elegant way")
    (clj-code
        "(defn my-map [f coll]"
        "    (reduce #(conj %1 (f %2)) [] coll))"))

(defslide other-map-like-function
    (markdown-part "#### many other map-like functions can be written similarly")
    (clj-code
        "(defn my-map [f coll]"
        "    (reduce #(conj %1 (f %2)) [] coll))")
    (clj-code
        "(defn my-filter [pred coll]"
        "    (reduce #(if (pred %2) (conj %1 %2) %1) [] coll))")
    (clj-code
        "(defn my-mapcat [pred coll]"
        "    (reduce (fn [r x](reduce conj r (f x))) [] coll))")
        )

(defslide the-main-issue
    (markdown-part
        "#### The main issue here the `conj`"
        " "
        "*  `conj` here yells \"collection\""
        "*  `conj` creates intermediate data structures everywhere"
        " "
    ))




(defslide revisiting-map-slides
    intro-slide
    naive-way
    clojuresque-way
    conj-wirdness
    reduce-way
    other-map-like-function
    the-main-issue
    )
