(ns transducerpres.slides.roadmap
    (:require [transducerpres.slides.util :refer [defslide fa]]))

(defslide roadmap-slide
    [:h3 "Road map " (fa "map")]
    [:ul
        [:li "What are transducers"]
        [:li "Use and create transducers"]
        [:li "A review of map function"]])
