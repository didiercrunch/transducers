(ns transducerpres.presentation
    (:require [transducerpres.slides.util :refer [defmarkdownslide]]
              [transducerpres.slides.intro :refer [intro-slide]]
              [transducerpres.slides.roadmap :refer [roadmap-slide]]
              [transducerpres.slides.what :refer [what-is-a-tranducer]]
              [transducerpres.slides.how :refer [how-to-create-slides]]
              [transducerpres.slides.revisiting-map :refer [revisiting-map-slides]]
              [transducerpres.slides.analogy :refer [rich-hickey-analogy]]
              [transducerpres.slides.transducer-function :refer [transducer-function-slides]]
              [transducerpres.slides.stopping :refer [stopping-slides]]
              [transducerpres.slides.state :refer [state-slides]]
              [transducerpres.slides.arities :refer [arities-slides]]
              [transducerpres.slides.completion :refer [completion-slides]]

        ))
(defmarkdownslide questions
    "questions?")

(def presentation
    [intro-slide
     roadmap-slide
     what-is-a-tranducer
     rich-hickey-analogy
     how-to-create-slides
     revisiting-map-slides
     transducer-function-slides
     stopping-slides
     state-slides
     arities-slides
     completion-slides
     questions
     ])
